<?php declare(strict_types=1);

namespace EasyCorp\Bundle\EasyAdminBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Controller\CrudControllerInterface;

/**
 * @author Troi
 */
abstract class AbstractDomainModelCrudController extends AbstractCrudController implements CrudControllerInterface
{
    abstract public static function getDomainModelFqcn(): string;
}
